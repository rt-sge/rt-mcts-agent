package at.ac.tuwien.ifs.sge.agent;

import at.ac.tuwien.ifs.sge.core.agent.AbstractRealTimeGameAgent;
import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.game.Game;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.core.util.tree.DoubleLinkedTree;
import at.ac.tuwien.ifs.sge.core.util.tree.Tree;

import java.util.*;
import java.util.concurrent.Future;

/*
    Real Time Monte Carlo Tree Search Agent

    A generic monte carlo tree search agent. Developed for demonstration purposes with the game CATCHY.

    Tries to periodically (decision pace) find the best action with monte carlo tree search on a tree that holds game states.
    A node in this tree represents a game state, the branches represent actions from this game state and each layer
    of the tree represents a point in time that advances by a defined interval (search pace) with the depth of the tree.
    Every layer of the tree in the simulation is alternately assigned a player id to also simulate the opponents.
    Furthermore, a simulation depth limit (search depth) is defined.

    The exploitation constant, decision pace, search pace and search depth should be adjusted to the specific game for reasonable results.

    This implementation copies the full game object for each tree node and is therefore NOT SUITED for games that have
    LARGE GAME STATES since it runs OUT OF MEMORY!
 */

public class RtMctsAgent<G extends RealTimeGame<A, ?>, A> extends AbstractRealTimeGameAgent<G, A> {

    /*
        The agent is started as a new process that gets passed three arguments:
            1. the player id of the player it is going to represent
            2. the player name of the player it is going to represent
            3. the game class name of the game it should play. This is only relevant if the agent supports multiple games.

        Since the 'Main-Class' attribute of the JAR file defined in the build.gradle points to this class
        a main method is defined which gets called when the engine starts the agent. The agent is then instantiated
        and started through the start() method, which also starts the agent's communication with the engine server.
    */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        var playerId = getPlayerIdFromArgs(args);
        var playerName = getPlayerNameFromArgs(args);
        var gameClass = ( Class<? extends RealTimeGame<Object, Object>> ) getGameClassFromArgs(args);
        var agent = new RtMctsAgent<>(gameClass, playerId, playerName, 0);
        agent.start();
    }

    private static final double DEFAULT_EXPLOITATION_CONSTANT = Math.sqrt(2);
    private static final int DEFAULT_SIMULATION_PACE_MS = 1000;
    private static final int DEFAULT_SIMULATION_DEPTH = 20;
    private static final int DEFAULT_DECISION_PACE_MS = 1000;

    private final Random random = new Random();
    private final double exploitationConstant;
    private final int simulationPaceMs;
    private final int simulationDepth;
    private final int decisionPaceMs;

    private Future<?> mctsIterationFuture;

    private final Comparator<Tree<RtMctsNode<A>>> gameMcTreeSelectionComparator;
    private final Comparator<Tree<RtMctsNode<A>>> gameMcTreeMoveComparator;

    private int nrOfPlayers;

    // Constructor with default parameters
    public RtMctsAgent(Class<G> gameClass, int playerId, String playerName, int logLevel) {
        this(DEFAULT_EXPLOITATION_CONSTANT,
                DEFAULT_SIMULATION_PACE_MS,
                DEFAULT_SIMULATION_DEPTH,
                DEFAULT_DECISION_PACE_MS,
                gameClass, playerId, playerName, logLevel);
    }

    /*
        A constructor calling the super constructor is required when extending the abstract agent.

        Since this agent can play any real time game the game class has a generic type.

        The comparators necessary to compare the game states according to monte carlo tree search are instantiated.
     */
    public RtMctsAgent(double exploitationConstant,
                       int simulationPaceMs,
                       int simulationDepth,
                       int decisionPaceMs,
                       Class<G> gameClass, int playerId, String playerName, int logLevel) {
        super(gameClass, playerId, playerName, logLevel);
        this.exploitationConstant = exploitationConstant;
        this.simulationPaceMs = simulationPaceMs;
        this.simulationDepth = simulationDepth;
        this.decisionPaceMs = decisionPaceMs;

        Comparator<Tree<RtMctsNode<A>>> gameMcTreeUCTComparator = Comparator
                .comparingDouble(t -> upperConfidenceBound(t, exploitationConstant));

        Comparator<RtMctsNode<A>> gameMcNodeVisitComparator = Comparator.comparingInt(RtMctsNode::getVisits);
        Comparator<RtMctsNode<A>> gameMcNodeWinComparator = Comparator.comparingInt(RtMctsNode::getWins);

        Comparator<RtMctsNode<A>> gameMcNodeGameComparator = (n1, n2) -> gameComparator.compare(n1.getGame(), n2.getGame());
        Comparator<Tree<RtMctsNode<A>>> gameMcTreeGameComparator = (t1, t2) -> gameMcNodeGameComparator
                .compare(t1.getNode(), t2.getNode());

        gameMcTreeSelectionComparator = gameMcTreeUCTComparator.thenComparing(gameMcTreeGameComparator);

        Comparator<RtMctsNode<A>> gameMcNodeMoveComparator = gameMcNodeVisitComparator.thenComparing(gameMcNodeWinComparator)
                .thenComparing(gameMcNodeGameComparator);
        gameMcTreeMoveComparator = (t1, t2) -> gameMcNodeMoveComparator
                .compare(t1.getNode(), t2.getNode());
    }

    /*
        The underlying structure of the abstract agent uses an ExecutorService to handle all its threads.
        The getMinimumNumberOfThreads() can be overridden to increases the number of threads the agent needs,
        but should always call the super method to ensure that the abstract agents threads are considered.

        This agent needs one additional thread.
     */
    @Override
    protected int getMinimumNumberOfThreads() {
        return super.getMinimumNumberOfThreads() + 1;
    }

    /*
        When the abstract agent receives an update from the server it is applied to the agents game after which
        the method onGameUpdate(A action, ActionResult result) is called. The parameters represent the
        action that has been applied as well as the result it created. The updated game state can be found
        in the protected field 'game' of the abstract agent implementation.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The agent does not react to game updates.
     */
    @Override
    protected void onGameUpdate(HashMap<A, ActionResult> actionsWithResult) {}

    /*
        In case of an action that was sent to the server and is not valid or no longer valid, the server responds
        with an InvalidActionEvent which contains the action that has been rejected. The abstract agent then
        calls the onActionRejected(A action) method.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The agent does not react to rejected actions.
     */
    @Override
    protected void onActionRejected(A action) {}

    /*
         After all agents have been started by the engine and the game has been initialized with the starting configuration
         all agents receive their game configuration (consisting only of the information they are allowed to have) and the
         number of players. Afterwards the setup(GameConfiguration gameConfiguration, int numberOfPlayers) method is called.
         When overriding the 'setup' method ALWAYS call the super method, to ensure that the game is instantiated with the
         right parameters and all fields are setup correctly.

         The unit types from the used configuration are stored in a seperate field.
    */
    @Override
    public void setup(GameConfiguration gameConfiguration, int numberOfPlayers) {
        super.setup(gameConfiguration, numberOfPlayers);
        this.nrOfPlayers = getGame().getNumberOfPlayers();
    }

    /*
        After every agent is setup, the engine server issues a StartGameEvent which indicates that it is now
        ready to receive and process actions from the agents.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The agent starts a new thread in which the computation happens.
     */
    @Override
    public void startPlaying() {
        mctsIterationFuture = pool.submit(this::play);
    }

    /*
        After the game is over the agent receives a TearDownEvent event, which signals it to stop
        the computation and shut down gracefully.

        The thread that runs the depth first search gets interrupted.
    */
    @Override
    public void shutdown() {
        mctsIterationFuture.cancel(true);
    }


    public void play() {
        A lastDeterminedAction = null;

        // Indefinitely try to find the best action of the current game state at a fixed decision pace
        for (;;) {
            try {

                // Copy the game state and apply the last determined action, since this action was not yet accepted and sent back
                // from the engine server at this point in time
                var gameState = copyGame();
                if (lastDeterminedAction != null && gameState.isValidAction(lastDeterminedAction, playerId)) {
                    gameState.scheduleActionEvent(new GameActionEvent<>(playerId, lastDeterminedAction, gameState.getGameClock().getGameTimeMs() + 1));
                }
                // Advance the game state in time by the decision pace since this is the point in time that the next best action will be sent
                gameState.advance(decisionPaceMs);

                // Create a new tree with the game state as root
                var mcTree = new DoubleLinkedTree<>(new RtMctsNode<>(playerId, gameState, null));
                // Tell the garbage collector to try recycling/reclaiming unused objects
                System.gc();

                var iterations = 0;
                // Calculate the time of the next decision
                var now = System.currentTimeMillis();
                var timeOfNextDecision = now + decisionPaceMs;

                log._info_();
                log.info("now = " + now);
                log.info("timeOfNextDecision = " + timeOfNextDecision);

                // Apply Monte Carlo Tree Search until the time of the next decision is reached
                while(System.currentTimeMillis() < timeOfNextDecision) {

                    // Select the best from the children according to the upper confidence bound
                    var tree = selection(mcTree);

                    // Expand the selected node by one action
                    tree = expansion(tree);

                    // Simulate until the simulation depth is reached and determine winners
                    var winners = simulation(tree, timeOfNextDecision);

                    // Back propagate the wins of the agent
                    backPropagation(tree, winners);

                    iterations++;
                }

                A action = null;
                if (mcTree.isLeaf()) {
                    log.info("Could not find a move! Doing nothing...");
                } else {
                    // Select the most visited node from the root node and send it to the engine server
                    log._info_();
                    log.info("Iterations: " + iterations);
                    for (var child : mcTree.getChildren())
                        log.info(child.getNode().getResponsibleAction() + " visits:" + child.getNode().getVisits() + " wins:" + child.getNode().getWins());
                    var mostVisitedNode = Collections.max(mcTree.getChildren(), gameMcTreeMoveComparator).getNode();
                    action = mostVisitedNode.getResponsibleAction();
                    log.info("Determined next action: " + action);
                    if (action != null)
                        sendAction(action, System.currentTimeMillis() + 50);
                }
                lastDeterminedAction = action;
            } catch (Exception e) {
                log.printStackTrace(e);
                break;
            } catch (OutOfMemoryError e) {
                break;
            }
        }
        log.info("stopped playing");
    }

    private Tree<RtMctsNode<A>> selection(Tree<RtMctsNode<A>> tree) {
        while (!tree.isLeaf()) {
            var bestChild = Collections.max(tree.getChildren(), gameMcTreeSelectionComparator);
            var node = tree.getNode();
            // if the node has unexplored actions compare it to the best child
            if (node.hasUnexploredActions()) {
                // if it has a better heuristic value keep exploring the root node even though it is no leaf
                if (gameMcTreeSelectionComparator.compare(tree, bestChild) >= 0)
                    break;
            }
            tree = bestChild;
        }
        return tree;
    }

    private Tree<RtMctsNode<A>> expansion(Tree<RtMctsNode<A>> tree) {
        var node = tree.getNode();
        var game = node.getGame();
        var playerId = node.getNextPlayerId();

        List<A> actions = new ArrayList<>();
        if (tree.isRoot()) {
            // when it's the root expand all possible actions
            while (node.hasUnexploredActions()) {
                actions.add(node.popUnexploredAction());
            }
            actions.add(null);
        } else {
            if (tree.isLeaf())
                // always explore the possibility of doing nothing first (null action)
                actions.add(null);
            else
                // otherwise, choose a random unexplored action
                actions.add(node.popUnexploredAction());
        }

        // Expand the tree by copying the game state and advancing it by the simulation pace
        Tree<RtMctsNode<A>> expandedTree = null;
        for (var action : actions) {
            var nextGameState = game.copy();
            if (action != null)
                nextGameState.scheduleActionEvent(new GameActionEvent<>(node.getPlayerId(), action, game.getGameClock().getGameTimeMs() + 1));
            try {
                nextGameState.advance(simulationPaceMs);
            } catch (ActionException e) {
                // If we have partial information (Fog of War) the result of some actions might be ambiguous leading in an ActionException
                // In that case just expand another action if there are any.
                if (!tree.isRoot() && node.hasUnexploredActions() && action != null) {
                    return expansion(tree);
                }
                continue;
            }
            expandedTree = new DoubleLinkedTree<>(new RtMctsNode<>(playerId, nextGameState, action));
            tree.add(expandedTree);
        }
        if (expandedTree == null) {
            return tree;
        }
        return expandedTree;
    }

    private boolean[] simulation(Tree<RtMctsNode<A>> tree, long timeOfNextDecision) {
        var node = tree.getNode();
        var game = node.getGame().copy();
        var playerId = node.getPlayerId();
        var depth = 0;
        try {
            // Simulate until the simulation depth or time of next decision is reached or the game is over
            while (!game.isGameOver() && depth++ <= simulationDepth && System.currentTimeMillis() < timeOfNextDecision) {
                // Apply a random action and advance the game state by the simulation pace
                var possibleActions = game.getPossibleActions(playerId);
                if (possibleActions.size() > 0) {
                    var nextAction = Util.selectRandom(possibleActions, random);
                    var doNothing = random.nextInt(possibleActions.size()) == 0;
                    if (!doNothing)
                        game.scheduleActionEvent(new GameActionEvent<>(playerId, nextAction, game.getGameClock().getGameTimeMs() + 1));
                }
                game.advance(simulationPaceMs);
                playerId = (playerId + 1) % game.getNumberOfPlayers();
            }
        } catch (ActionException e) {
            // If we have partial information (Fog of War) the result of some actions might be ambiguous leading in an ActionException
            // Stop the simulation there
            log.debug("simulation reached invalid game state (partial information)");
        } catch (Exception e) {
            log.printStackTrace(e);
        }
        return determineWinner(game);
    }

    private void backPropagation(Tree<RtMctsNode<A>> tree, boolean[] winners) {
        // Go back up in the tree and increment the visits of evey node as well as the wins of the players nodes
        do {
            var node = tree.getNode();
            node.incrementVisits();
            var playerId = (node.getPlayerId() - 1);
            if (playerId < 0) playerId = nrOfPlayers - 1;
            if (winners[playerId])
                node.incrementWins();
            tree = tree.getParent();
        } while (tree != null);
    }

    // Determines the game states 'winners' based on either the utility value or the heuristic value
    private boolean[] determineWinner(Game<A, ?> game) {
        var winners = new boolean[game.getNumberOfPlayers()];
        if (game.isGameOver()) {
            var evaluation = game.getGameUtilityValue();
            for (var pid = 0; pid < game.getNumberOfPlayers(); pid++)
                if (evaluation[pid] == 1D)
                    winners[pid] = true;
        } else {
            var evaluation = game.getGameHeuristicValue();
            var maxIndex = 0;
            for (var pid = 1; pid < game.getNumberOfPlayers(); pid++) {
                if (evaluation[pid] > evaluation[maxIndex])
                    maxIndex = pid;
            }
            winners[maxIndex] = true;
        }
        return winners;
    }

    // Calculates the upper confidence bound (UCB) from mcts node
    private double upperConfidenceBound(Tree<RtMctsNode<A>> tree, double c) {
        double w = tree.getNode().getWins();
        double n = Math.max(tree.getNode().getVisits(), 1);
        double N = n;
        if (!tree.isRoot()) {
            N = tree.getParent().getNode().getVisits();
        }

        return (w / n) + c * Math.sqrt(Math.log(N) / n);
    }

}
