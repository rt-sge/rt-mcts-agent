package at.ac.tuwien.ifs.sge.agent;

import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;
import at.ac.tuwien.ifs.sge.core.util.node.RealTimeGameNode;

public class RtMctsNode<A> extends RealTimeGameNode<A> {


    private int wins;
    private int visits;


    public RtMctsNode(int playerId, RealTimeGame<A, ?> game, A responsibleAction) {
        super(playerId, game, responsibleAction);
        wins = 0;
        visits = 0;
    }


    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getVisits() {
        return visits;
    }

    public void setVisits(int visits) {
        this.visits = visits;
    }

    public void incrementWins() {
        wins++;
    }

    public void incrementVisits() {
        visits++;
    }

}
